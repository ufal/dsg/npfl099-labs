#### What component you'll implement

I will implement Intent Classification using BERT as mentioned in the paper (Qian et al., 2019). 

#### What model architectures you'll use -- a short description of how the model architectures will look like (for all the model variants), with citation(s) of relevant paper(s)

I will be implementing two model architectures. Support Vector Machines and BERT. I expect BERT to outperform SVM given the BERT's tendency to generalize better. 

##### Support Vector Machines
Support vector machines is a supervised learning algorithm that uses kernal functions to classify data. Most probably, I will be using Linear SVC. Input vector for SVM will be a tf-idf vector. 

##### BERT: Bidirectional Encoder Representation using Transformers

BERT (Jacob et al., 2018) is a language representation model which is based on the original Transformer architecture (Vaswani et al., 2017). Unlike other neural architectures like Recurrent Neural Networks (Elman, 1990) which process the input unidirectionaly, BERT uses Masked Language Modeling to process inputs Bi-directionaly. As mentioned in the paper (Qian et al., 2019), BERT used for intent classification looks as follows:

12 Layers

768 Hidden states

12 Heads. 

Max Length is 50

Learning rate 5e-5

Dropout rate 0.1

Optimizer is Adam (Kingma and Ba, 2014) 

All the hyper parameters will be tuned to the development set.
 
#### What dataset(s) you'll use, any potential modifications required for the dataset

The dataset I will be using is one of the datasets mentioned in the paper: Airline Traffic Information System (ATIS). 

Annotation example: 
| Intent | Sentence |
| ------ | ------ |
| atis_flight | what flights are available from pittsburgh to baltimore on thursday morning |
| atis_airfare | cheapest airfare from tacoma to orlando |
| atis_ground_service  | what kind of ground transportation is available in denver  |
| atis_airline  | show me the airlines with first class flights  |
|  atis_abbreviation | what does the fare code f mean  |

Percentage of all annotations:
| Intent | Percentage |
| ------ | ------ |
| atis_flight | 74% |
| atis_airfare | 8% |
| Other | 18% |

#### What evaluation metrics you'll use

For Support Vector Machine, the evaluation metrics will be Accuracy and F1 score.
For BERT, evaluation metrics will be Accuracy and F1 score. 


#### Citations

1. BERT for Joint Intent Classification and Slot FillingQian Chen∗, Zhu Zhuo, Wen Wang

2. BERT: Pre-training of Deep Bidirectional Transformers for Language Understanding
Jacob Devlin, Ming-Wei Chang, Kenton Lee, Kristina Toutanova 

3. Attention Is All You Need Ashish Vaswani, Noam Shazeer, Niki Parmar, Jakob Uszkoreit, Llion Jones, Aidan N. Gomez, Lukasz Kaiser, Illia Polosukhin

4. Jeffrey L. Elman.  Finding structure in time.Cognitive Science, 14(2):179–211, March 1990

5. ATIS dataset https://www.kaggle.com/hassanamin/atis-airlinetravelinformationsystem
