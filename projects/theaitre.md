#### What component you'll implement

We will implement an end-to-end dialog system (actually several of them, but they will most likely be instances of the same model trained on differently labeled data).  
The main goal is to produce a theatre play with consistent characters.

#### What model architectures you'll use -- a short description of how the model architectures will look like (for all the model variants), with citation(s) of relevant paper(s)

We will most likely try to finetune a model like GPT2 (Radford et al.) or DialoGPT (Zhang et al. 2020)
We would like to be able to feed the model some context, or perhaps to give chatbot A information about chatbot B in order to make the characters more consistent. (Zhang et al. 2018).
Possibly by feeding GPT with context or extracting the Hidden state and use it as an initial hidden_state.

We will most likely need a model to determine which character will speak in the next turn - possibly inspired by Moretti, otherwise a "smarter random" approach.
We could do both and compare them.


#### What dataset(s) you'll use, any potential modifications required for the dataset

We will use a dataset of theatre plays or scriptbase, since it is easier to work with. The potential modifications include:
- Selection of the plays with a desirable number of characters (2 - 4, 3 is best)
- Label modification (change characters into ‘focus’ and ‘other’)
- Character clustering - in order to train the model on several games, it might be reasonable to assign similar characters to it
- Fun experiment: try to perform clustering based on sentiment in an attempt to cluster by personality type
- Another approach towards character clustering: https://www.sbert.net/ Is a great tool. Dominik really wants to use this. (somehow) He would like to somehow use a Bi-Encoder define a suitable loss ( hardest part) This would effectively learn embeddings of persons.
- The baseline idea is to filter scenes with 2 people and use a siamese network and learn the embedding (Dominik read in the writeup that clustering bert is too straightforward but this is kinda overkill?)

#### What evaluation metrics you'll use

To evaluate the character consistency, we would like to perform manual evaluation.  
For example evaluators (ideally dramaturges or DAMU students) would select whether the characters are more consistent in our version or the baseline version (for the baseline we will use the deployed THEaiTRE model). For this, we would select chunks of 10 - 20 turns.

We will also use metrics such as the BLEU score or perplexity, possibly other statistical metrics - this will not really be an issue.

nice to have: We will try to determine whether there is a plot twist in the generated dialogs

potential evaluation: Train a contrastive discriminator on movie- dialogue and plain-text , and evaluate on its outputs.


#### Team competences and responsibilities
- Dominik will perform character clustering based on sBert
- Christián will perform character clustering based on sentiment and also look at Moretti.
- Patrícia will feed the model with context.
- Dávid will look at finetuning the models.

We realize that these overlap and that everyone will probably look at everything at a certain point, but this is a nice initial division that everyone was happy with.

#### Citations:
Alec Radford, Jeff Wu, Rewon Child, David Luan, Dario Amo-dei, and Ilya Sutskever.Language models are unsupervised mul-titask learners.Technical report, OpenAI, 2019.URLhttps://cdn.openai.com/better-language-models/language_models_are_unsupervised_multitask_learners.pdf


Yizhe Zhang, Siqi Sun, Michel Galley, Yen-Chun Chen, Chris Brockett, Xiang Gao, Jianfeng Gao, Jingjing Liu, and Bill Dolan. Dialogpt: Large-scale generative pre-training for conversational response generation, 2020.

Saizheng Zhang, Emily Dinan, Jack Urbanek, Arthur Szlam, Douwe Kiela, and Jason Weston. Personalizing dialogue agents: I have a dog, do you have pets too? In Proceedings of the 56th Annual Meeting of the Association for Computational Linguistics (Volume 1: Long Papers), pages 2204–2213, Melbourne,Australia, July 2018. Association for Computational Linguistics. doi: 10.18653/v1/P18-1205. URL https://www.aclweb.org/anthology/P18-1205

Pinelopi Papalampidi, Frank Keller, and Mirella Lapata.  Movie plot analysis via turning point identification.  In Proceedings of the 2019 Conference on Empirical Methods in Natural Language Processing and the 9th International Joint Conference on Natural Language Processing (EMNLP-IJCNLP), pages 1707–1717, Hong Kong, China, November 2019. Associ-ation for Computational Linguistics.  doi: 10.18653/v1/D19-1180.  URL https://www.aclweb.org/anthology/D19-1180
