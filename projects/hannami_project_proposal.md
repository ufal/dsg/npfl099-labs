# Project Description
Michael Hanna

### Project Statement
For my project, I intend to build on Wu et al. (2020)'s paper on TOD-BERT, Task-Oriented Dialogue BERT. TOD-BERT is a version of BERT (Devlin et al., 2019) that has been pre-trained on a variety dialogue specific datasets, all of which are comprised of system-user interactions. Specifically, it consists of already pretrained BERT (using masked language modeling (MLM) and next sentence prediction), pretrained further on these dialogue datasets using MLM and a contrastive objective. Much like the original BERT, once thus pretrained, TOD-BERT can be used on a number of dialogue systems tasks, including intent recognition, dialogue state tracking, dialogue act prediction, and response selection.

To build on this work, I have a number of planned goals; I am committed to completing the first, but hope to also do work on some of the later, more ambitious goals as well. These goals (roughly in the order I would like to complete them) are:

 1. Re-train TOD-BERT, starting from RoBERTa's (Liu et al., 2019) weights, rather than BERT's
 2. Experiment with different contrastive objectives for training TOD-BERT
 3. Experiment with different task-specific architectures (that trained on top of / fine-tuned with TOD-BERT) 
 4. Perform a more in-depth analysis (via probing) of the representations learned by TOD-BERT (this was done only briefly in the original paper)

Note that in the original paper, the authors use four different tasks (discussed below) to evaluate TOD-BERT. Depending on how dificult it is to perform evaluation, I will try to evaluate on all of these tasks, but if its is not possible, I will focus on dialogue state tracking, with response selection as a secondary focus.

### Dialogue System Component
Similar the original BERT model, which could perform a variety of NLP tasks depending on the architecture that was added on top of it, TOD-BERT can perform tasks associated with a number of different components in the dialogue system pipeline. In the original paper, the authors use it to perform the following tasks: 

- intent recognition: part of the NLU process
- dialogue state tracking: part of the dialogue management process
- dialogue act prediction: part of the dialogue management process
- response selection: part of the NLG process

### Model Architectures

As mentioned previously, TOD-BERT uses BERT's fine-tuning approach to tackling different downstream tasks. That is, to use TOD-BERT on a downstream task, one simply puts simple model architectures on top of TOD-BERT, feeding TOD-BERT's representations into these models. Then, the model is trained (and TOD-BERT finetuned) on this downstream task.

To review, the architecture of BERT works as follows. First, sentences are fed into BERT, split via WordPiece (Wu et al., 2016) segmentation; distinct sentences are separated by special [SEP] token, and the entire input has a special [CLS] token prepended. Then, each token is given a representation combining token, segment / sentence identity, and positional information. Then, these representations are fed into many layers of Transformers (Vaswani et al., 2017); each layer of Transformers uses bidirectional attention over the entire input to compute a representation for each token in the given layer. After many repeated applications of self-attention, BERT outputs representations for each token in the input.

Crucially for these downstream tasks, BERT outputs a representation of the special [CLS] token, which is often, including in this paper, used as a representation of the entire input. This is relevant for some of the following tasks from the paper, whose corresponding architectures I now describe. 

- intent recognition: given the most recent user utterance, score potential intents by feeding the utterance's representation into a linear model, then generate a probability distribution over potential intents using softmax.
- dialogue state tracking: this task is somewhat complex. For each slot, and for each value possible therein, a fixed representation is computed using TOD-BERT prior to fine-tuning. Then, given the dialogue history, the distribution over values for a given slot is proportional to the value calculated as follows. First, the representation of the history is passed through a slot projection layer specific to the slot in question. Then, the score of a value for that slot is proportional to the cosine distance between that value's representation and the projected representation of the history.
- dialogue act prediction: given the dialogue history, score each dialogue act by feeding its representation into a linear model, and then generate probabilities by applying a sigmoid function element-wise; a dialogue act is considered activated if its probability is > 0.5, and multiple dialogue acts can be predicted at once.
- response selection: given the dialogue history, and a number of candidate responses, calculate the cosine similarity between the history's representation and each candidate's represenation, choosing the candidate with maximum similarity

### Datasets
In the original work, the authors evaluate their model on four tasks, and four dataset; in my follow-up, I intend to do the same. Some datasets are used for multiple tasks. The datasets (annotated with (#train/#val/#test) examples) used are:

- OOS (Larson et al. 2019): used for intent recognition, contains user utterances and corresponding intents. Contains some out-of-scope intents, that do not fit into the predefined intent categories. (15100/3100/5500)
- DSTC2 (Henderson et al., 2014): human-machine task-oriented dialogue corpus. Its dialogue acts need to be mapped to universal dialogue acts. (1612/506/1117)
- GSIM (Shah et al., 2018): machine-machine task-oriented dialogue corpus that was rewritten by humans. Consists of dialogues for two domains, movies and restaurants, that will be merged together. Its dialogue acts need to be mapped to universal dialogue acts. (1500/469/1039)
- MWOZ 2.1 (Budzianowski et al., 2018): human-human dialogue corpus spanning many domains / topics. Especially useful for dialogue state tracking, as it is annotated with the slots information need for this task. (8420/1000/1000)

And the task-dataset correspondences are as follows:
- intent recognition: OOS
- dialogue state tracking: MWOZ
- dialogue act prediction: DSTC2, GSIM, MWOZ
- response selection: DSTC2, GSIM, MWOZ

These choices are due to the fact that OOS is the only intent recognition dataset, while MWOZ is annotated with information needed for dialogue state tracking.

### Evaluation Metrics
In all of the 4 aforementioned tasks, accuracy, precision, recall, and F1 can be used in evaluation. Of course, for each task, they have a different usage:

- intent recognition: We evaluate accuracy on in-domain intents. On out-of-domain tasks, recall is of interest.
- dialogue state tracking: We use acuracy, either assigning credit for each slot-value pair where the predicted value is correct, or only when all predicted values are correct
- dialogue act prediction: Since this a multi-label task (we may want to produce multiple dialogue acts in our response to a given history), we use F1 score to score our label predictions.
- response selection: accuracy@N is used. Given a dialogue history, the model predicts the N most likely responses of 100 responses; if the prediction is in the top N, the model is correct. N can be small, e.g. 1 or 3.


### Baselines
As part of this project, we will compare our model's performance on the aforementioned tasks to pre-existing models. First and foremost of these will be the original TOD-BERT; the original BERT/RoBERTa, used to generate predictions in the same way as TOD-BERT, are also good candidates. Finally, DialoGPT / GPT2 are additional strong baselines presented in the original TOD-BERT paper that will provide context for our results. It is possible to use other types of models as baselines (e.g. baselines that are not pretrained (M)LMs); however, by using the same baseline model on many tasks, as opposed to using a totally new architecture for each task, we provide a stong and fair baseline that functions similarly to our model.
  

## Works Cited

Budzianowski, Paweł, Tsung-Hsien Wen, Bo-Hsiang Tseng, Inigo Casanueva, Stefan Ultes, Osman Ramadan, and Milica Gašić. "Multiwoz-a large-scale multi-domain wizard-of-oz dataset for task-oriented dialogue modelling." _arXiv preprint arXiv:1810.00278_ (2018).

Devlin, Jacob, Ming-Wei Chang, Kenton Lee, and Kristina Toutanova. "Bert: Pre-training of deep bidirectional transformers for language understanding." _arXiv preprint arXiv:1810.04805_ (2018).

Henderson, Matthew, Blaise Thomson, and Steve Young. "Word-based dialog state tracking with recurrent neural networks." In _Proceedings of the 15th Annual Meeting of the Special Interest Group on Discourse and Dialogue (SIGDIAL)_, pp. 292-299. 2014.

Larson, Stefan, Anish Mahendran, Joseph J. Peper, Christopher Clarke, Andrew Lee, Parker Hill, Jonathan K. Kummerfeld et al. "An evaluation dataset for intent classification and out-of-scope prediction." _arXiv preprint arXiv:1909.02027_ (2019).

Liu, Yinhan, Myle Ott, Naman Goyal, Jingfei Du, Mandar Joshi, Danqi Chen, Omer Levy, Mike Lewis, Luke Zettlemoyer, and Veselin Stoyanov. "Roberta: A robustly optimized bert pretraining approach." _arXiv preprint arXiv:1907.11692_ (2019).

Shah, Pararth, Dilek Hakkani-Tur, Bing Liu, and Gokhan Tur. "Bootstrapping a neural conversational agent with dialogue self-play, crowdsourcing and on-line reinforcement learning." In _Proceedings of the 2018 Conference of the North American Chapter of the Association for Computational Linguistics: Human Language Technologies, Volume 3 (Industry Papers)_, pp. 41-51. 2018.

Vaswani, Ashish, Noam Shazeer, Niki Parmar, Jakob Uszkoreit, Llion Jones, Aidan N. Gomez, Łukasz Kaiser, and Illia Polosukhin. "Attention is all you need." In _Advances in neural information processing systems_, pp. 5998-6008. 2017.

Wu, Chien-Sheng, Steven Hoi, Richard Socher, and Caiming Xiong. "Tod-bert: Pre-trained natural language understanding for task-oriented dialogues."  _arXiv preprint arXiv:2004.06871_  (2020).

Wu, Yonghui, Mike Schuster, Zhifeng Chen, Quoc V. Le, Mohammad Norouzi, Wolfgang Macherey, Maxim Krikun et al. "Google's neural machine translation system: Bridging the gap between human and machine translation." _arXiv preprint arXiv:1609.08144_ (2016).