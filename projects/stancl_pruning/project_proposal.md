## Weight Pruning for Transformer-based Models in Dialogue Systems.

Weight pruning is a technique that is used for reducing a model size without sacrificing accuracy, and researchers are usually capable to decrease the number of model parameters up to 90 % [\[Frankle and Carbin (2018)\]](https://arxiv.org/abs/1803.03635). Following the results of _The Lottery Ticket Hypothesis_ [[\[Frankle and Carbin (2018)\]](https://arxiv.org/abs/1803.03635)], a lot of endeavours have been dedicated to the weight pruning, and some researchers have already shown this technique to be proficient in transformer-based models for Natural Language Understanding (NLU) too ([\[Yu et al., (2019)\]](https://arxiv.org/abs/1906.02768), [\[Gordon et al., (2020)\]](https://arxiv.org/abs/2002.08307), [\[Sanh et al., (2020)\]](https://arxiv.org/abs/2005.07683) [\[Li et al., (2020)\]](https://arxiv.org/pdf/2009.08065.pdf)). Furthermore, [Voita et al., (2019)](https://arxiv.org/abs/1905.09418), [Michel et al., (2019)](https://proceedings.neurips.cc/paper/2019/hash/2c601ad9d2ff9bc8b282670cdd54f69f-Abstract.html) or [Hou et al., (2020)](https://proceedings.neurips.cc/paper/2020/hash/6f5216f8d89b086c18298e043bfe48ed-Abstract.html) investigated the utilization of weight pruning specifically for attention heads in multi-head attentions. In all cases, the authors provided supporting evidence for the efficacy of weight pruning of heads in the multi-head attention (MHA).

While BERT-like models (BERT [\[Devlin et al., (2018)\]](https://arxiv.org/abs/1810.04805), RoBERTa [\[Liu et al., (2019)\]](https://arxiv.org/abs/1907.11692), Electra [\[Clark et al., (2020)\]](https://arxiv.org/abs/2003.10555) and many others) are not generators in nature as they lack a decoder, successful attempts to adapt these models to NLG tasks, such as dialogues, machine translation or summarization, have already appeared starting with BART introduced by [Lewis et al., (2019)](https://arxiv.org/abs/1910.13461). This new model can be seen as a generalization of BERT.

Both weight pruning and mainly text generation with BART are relatively recent methods, therefore, although weight pruning has proved to be proficient in NLU tasks, there is only a little or zero published research efforts in NLG. This study thus aims to fill this void and make a similar study to [Voita et al., (2019)](https://arxiv.org/abs/1905.09418) just in NLG scenarios.

---------


**Model:**

- BART introduced in [Lewis et al., (2019)](https://arxiv.org/abs/1910.13461) in a compressed version with 6 hidden stacked layers both in encoder and decoder and 8 heads in MHA following the setting of the very initial transformer presented by [Vaswani et al., (2017)](https://proceedings.neurips.cc/paper/2017/hash/3f5ee243547dee91fbd053c1c4a845aa-Abstract.html). The performance of this compressed model is to be compared with the "base" BART from [Lewis et al., (2019)](https://arxiv.org/abs/1910.13461) possessing with 12 layers and 16 heads in each MHA.

**Data:** _(tentative)_

- PersonaChat (http://convai.io/#personachat-convai2-dataset) 
- Ubuntu Dialogue Corpus (https://github.com/rkadlec/ubuntu-ranking-dataset-creator)

**Observed metrics:**

- Some standard metrics from the set of {BLEU, ROUGE, METEOR} are to be observed. These measures, however, are not to be crucial (per se maximized or so) and they will be rather used just for the control that accuracy of pruned models will not drop too much. (The ultimate goal still will be to prune a model without excessively harming the model performance. Nevertheless, the aim of this paper is not as ambitious as reaching SOTA results.)
- Besides these standard measures, error analyses of generated dialogues are going to be also conducted.