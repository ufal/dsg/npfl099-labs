Hints on Writing
================

This is by no means a good guide to academic writing, just a set of hints that you should keep in mind when writing reports, papers, or theses:

* **Be clear**. This is the most important rule of all. Think of the reader and try to make yourself understood. 
  If a (reasonably intelligent and educated) reader doesn't understand your paper, it's *your* fault, not theirs.

  * This might be different in e.g. Czech academic writing, but in English, clarity is the main point.

  * Don't add surprises -- state your purpose right in the abstract and introduction. Reveal the findings straight away.

  * Don't worry too much about repeating the same words. If it's clearer, it's better -- you're not writing a sonnet (and you're not writing in Czech).

  * Go straight to the point -- don't overuse abstract & obscure words (method, approach, etc.).

  * Don't assume anyone has read the related works. If an approach from a related paper is important to your own, you need to explain it.
     Don't brush off the explanation by just using a few keywords from the title of that related paper -- make sure the reader has an idea what
     related paper is really about, what it's really doing. Feel free to simplify that idea to make it clearer.


* **Be orderly**. This is actually a part of being clear, but a specific one.

  * Think about how you order the facts you present in a given section. In an ideal case, each new sentence/fact should follow from the previous
     ones.

  * Paragraphs rule of thumb: each paragraph should be about one thing (concept, step in your procedures etc.).
  
  * Make sure all new concepts are properly introduced (and cited). Feel free to invent your names for new concepts, but stick to them then.

  * Refer to related parts of the paper (e.g., if you mention a value for a parameter, refer to a section where you introduced a parameter). Refer
     to all figures and tables from the text -- they can't just hang randomly on the page, they need to be connected. References use capital letters
     and no articles -- “Figure 1, Section 5.3, Table 7”.

  * Try to introduce concepts from basic to more complex, so that you don't need to refer forward in the paper.


* **Stick to convention**. Follow the typical structure of papers:

    * *Abstract* -- this should state the most important points, including the findings. Do not overdo it with the background here -- don't start with
      “NLU is an important component in dialogue systems”, start with “We built an NLU system based on transformer language models.”

    * *Introduction* -- this is basically an extended abstract, with more background, and more details on what exactly you do in the paper.
      It should contain a list of your key findings.

    * *Related work* -- mention other works that are most relevant to your approach. How broad you go depends on the paper (a few most important ones
      are OK for your report in this course, but you'll need to go much broader in your thesis). Especially in papers, make sure you describe
      what's similar and different w.r.t. to your own work.
      
      In some papers, you'll find the related works after the results section. This is also OK, if it works better for you.

    * *Your approach / model architecture* -- try to explain the details of your model as clearly as possible here. A picture or a schema may help a lot.

    * *Experiments* -- datasets, model parameters/settings, evaluation metrics.

    * *Results* -- the numbers, and a discussion (why did you get these numbers? where does your model work well and where does it fail?). Try to think 
      about the results. This is really crucial, and brushing off results with a table of numbers won't be accepted.

      Note that your report should include some manual evaluation -- this is to help you with the results discussion.

    * *Conclusion* -- just a short summary of what you did, what were the key findings, and future works (more like your ideas for improvement, you're
      not promising that you'll actually do it).


* It might be best to **start with structure and notes**, rather than write from the beginning to the end.

    * The notes might help you start faster -- you don't stare at an empty page for long. 

    * When you just have notes, it's easy to reorder them and group them into what later becomes paragraphs.
    
    * It might be easier to write sloppily at the start, then gradually polish it.

    * It might be good to write the abstract and introduction last, after you've finished with describing your model and the paper.
       

* **Cite** properly.

    * Mentioning author names is much better than using numbers in brackets (e.g., I know what (Vaswani et al., 2017) is, but [4] doesn't say much).

    * If you do work with author names in brackets, distinguish `\citep` and `\citet` in natbib scheme, depending on whether the authors' names
      are part of the sentence or not. E.g. “Vaswani et al. (2017) show that...” vs. “Transformer models (Vaswani et al., 2017) brought...”

    * Conferences/journals are more important than arXiv -- if a paper was published at a conference or in a journal, you should cite the published
      version. This might be hard to find out, but if the arXiv page or the paper itself mentions a conference/journal, go for that.

    * Make it clear whenever you quote other papers directly (though you probably won't be doing this often).


* Use a spelling and/or a grammar checker. Note that specific terms might not be in the dictionary.


* Writing papers is a skill, not an art -- practice helps :-).


Further
-------
* This is [a nice and concise Twitter thread](https://twitter.com/caranha/status/1290606251529797632) about paper writing.
* Some further helpful guides, though a little generic:
  * https://collegeinfogeek.com/write-excellent-papers-quickly/
  * https://www.washingtonpost.com/blogs/answer-sheet/post/a-guide-to-writing-an-academic-paper/2012/01/18/gIQAjGCTCQ_blog.html

