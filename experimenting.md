Some hints on your experiments with neural nets
===============================================

Frameworks & tutorials
----------------------

* You can use any framework you want, but there are 2 major ones: PyTorch & Tensorflow. 
* Tensorflow is probably more widespread in the industry, PyTorch is more used in research.
* Both are good and both have their quirks, PyTorch seems a bit more user-friendly to me.


For PyTorch:
* There are some [very nice tutorials right on the PyTorch website](https://pytorch.org/tutorials/)
    * to actually enter the tutorial, click “Next” on the bottom of the page
* To understand what happens under the hood, check out [MiniTorch](https://minitorch.github.io/index.html)
    * Python-only reimplementation of PyTorch basics
* To write less code, you may want to check out the high-level [PyTorch Lightning library](https://www.pytorchlightning.ai/)


For Tensorflow:
* Google has a [nice and detailed tutorial here](https://codelabs.developers.google.com/codelabs/cloud-tensorflow-mnist/)


Any other good learning aids? Let us know on Slack!

Training your network
---------------------

* **Start small, go slow, don't trust the model**
    * NNs are a huge structure and basically anything can go wrong
    * Start with the simplest version of the model
    * Test parts of the model in isolation

* Overfit a toy dataset, then overfit the training set
    * **Build a toy dataset** of [10-100 easy examples](https://github.com/tuetschek/ratpred/blob/master/experiments/data/toy/train.tsv)
      where you're sure the model should pick it up
    * Try to overfit it -- does the model get 100% accuracy? Only after it does, you should move to real data
        * note that you might need to change parameters (make the net smaller) for this
    * Debugging on toy datasets is fast -- you don't have to wait hours for results
    * Before you do the actual experiments, make sure your model also trains on the training data -- loss goes down

* Then work on the large data -- regularize (dropout, data augmentation, pretraining)
    * Make sure your model works well on a heldout/dev set
    * Do not look at the test set -- that's for your very final experiments only (ideally, you should throw it away after use)

* Deal with the randomness
    * **Fix your random seeds** so repeated experiments only show differences in what you changed in the model,
      not in the random number generator
    * For your final models, if they aren't too big, run multiple runs with different random seeds and 
      average the results

* Test out changes one at a time
    * That way you'll know what change breaks it (or makes a breaktrough)

* Know your data
    * **Inspect your dataset** before training -- that way you'll know where some errors might come from
    * Don't forget about checking & debugging your own preprocessing
        * make sure you're not leaking more information to the model than you should
        * if there's a way to cheat, your model will use it
    * Check the [sources of your data](https://twitter.com/r_speer/status/1298297872228786176?s=09)

* **Look at the outputs** (on development data)
    * Looking at scores is not nearly enough -- by looking at the actual outputs, you'll see the _kinds of mistakes_
      your model makes -- it's a much better guidance in deciding what to change in the model.

Read more & better at [Andrej Karpathy's blog](http://karpathy.github.io/2019/04/25/recipe/).


Experiment management
---------------------

Main point is -- **store everything from one experiment together** so you can make sense of your experiments!
Once you run more than a couple, you won't remember which configuration and/or code version is which.

Best practice: start a new directory for each experiment, which should contain:
* Your configuration
* Some information about your code version (e.g. last git commit + local changes)
* Some information about the version of input data you're using (or the copy of the whole data, if it's not too big)
* Your trained models (if space allows)
* Outputs of your system
* Outputs of score computation

You can use [Weights & Biases](https://wandb.ai/) to keep track of your experiments in a nicely formatted webpage -- see more 
details in the lab video. W&B also plugs nice with Tensorboard.

As a slightly more low-tech option, creating a makefile to automate creating your experiment directories is also a good option.
You can look at [Ondrej's makefiles here](https://github.com/tuetschek/ratpred/blob/master/experiments/Makefile), but beware 
of undocumented code :-). This probably best to do your way anyway. There's some more commentary on this in the lab video, too.
