# MS Dialogue Challenge Data

## Dataset information
The dataset consists of 3 parts:
* movie-tickets booking dialogues,
* restaurant reservation dialogues,
* taxi ordering dialogues.

All subdatasets are in textual form, and the data are stored in 3 `.tsv` files. The dialogues were collected using crowdsourcing platform from Amazon - Amazon Mechanical Turk. The data is designed for creating end-to-end text based dialogue system with NLU, NLG and DM modules. 

Each dialogue is fully annotated with dialogue acts so each turn contain some intent and maybe slots with values (usually turns with greeting/thanking/... intents have no slots and values). Below are shown some basic counts for the dataset.
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0pky"></th>
    <th class="tg-0pky">restaurant dataset</th>
    <th class="tg-0pky">taxi dataset</th>
    <th class="tg-0pky">movie dataset</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">Dialogues count</td>
    <td class="tg-0pky">4103</td>
    <td class="tg-0pky">3094</td>
    <td class="tg-0pky">2890</td>
  </tr>
  <tr>
    <td class="tg-0pky">Turns count</td>
    <td class="tg-0pky">29719</td>
    <td class="tg-0pky">23311</td>
    <td class="tg-0pky">21656</td>
  </tr>
  <tr>
    <td class="tg-0pky">Sentences count</td>
    <td class="tg-0pky">44289</td>
    <td class="tg-0pky">32726</td>
    <td class="tg-0pky">31305</td>
  </tr>
  <tr>
    <td class="tg-0pky">Words count</td>
    <td class="tg-0pky">337047</td>
    <td class="tg-0pky">256623</td>
    <td class="tg-0pky">236480</td>
  </tr>
  <tr>
    <td class="tg-0pky">Intents count</td>
    <td class="tg-0pky">11</td>
    <td class="tg-0pky">11</td>
    <td class="tg-0pky">11</td>
  </tr>
  <tr>
    <td class="tg-0pky">Slots count</td>
    <td class="tg-0pky">30</td>
    <td class="tg-0pky">19</td>
    <td class="tg-0pky">29</td>
  </tr>
  <tr>
    <td class="tg-0pky">Vocabulary size</td>
    <td class="tg-0pky">9489</td>
    <td class="tg-0pky">8784</td>
    <td class="tg-0pky">6534</td>
  </tr>
  <tr>
    <td class="tg-0pky">User entropy</td>
    <td class="tg-0pky">5.906</td>
    <td class="tg-0pky">5.986</td>
    <td class="tg-0pky">5.692</td>
  </tr>
  <tr>
    <td class="tg-0pky">Agent entropy</td>
    <td class="tg-0pky">5.632</td>
    <td class="tg-0pky">5.236</td>
    <td class="tg-0pky"><span style="font-weight:400;font-style:normal">5.812</span></td>
  </tr>
</tbody>
</table>

It can be seen, that the restaurant reservation dataset has more dialogues, thus larger vocabulary size, more sentences and more turns. The entropy of user/agent turns in all datasets is very similar.

The next table shows some basic statistics measured in the dialogues.

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0pky"></th>
    <th class="tg-0pky"></th>
    <th class="tg-c3ow" colspan="3">mean</th>
    <th class="tg-c3ow" colspan="3">standard deviation</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky"></td>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">restaurant dataset</td>
    <td class="tg-0pky">taxi dataset</td>
    <td class="tg-0pky">movie dataset</td>
    <td class="tg-0pky">restaurant dataset</td>
    <td class="tg-0pky">taxi dataset</td>
    <td class="tg-0pky">movie dataset</td>
  </tr>
  <tr>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">turns per dialogue</td>
    <td class="tg-0pky">7.243</td>
    <td class="tg-0pky"><span style="font-weight:400;font-style:normal">7.534</span></td>
    <td class="tg-0pky">7.493</td>
    <td class="tg-0pky">3.396</td>
    <td class="tg-0pky">3.253</td>
    <td class="tg-0pky">3.366</td>
  </tr>
  <tr>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">sentences per dialogue</td>
    <td class="tg-0pky">10.794</td>
    <td class="tg-0pky">10.577</td>
    <td class="tg-0pky">10.832</td>
    <td class="tg-0pky">5.074</td>
    <td class="tg-0pky">4.349</td>
    <td class="tg-0pky">5.15</td>
  </tr>
  <tr>
    <td class="tg-0pky"></td>
    <td class="tg-0pky">words per dialogue</td>
    <td class="tg-0pky">82.146</td>
    <td class="tg-0pky">82.942</td>
    <td class="tg-0pky">81.827</td>
    <td class="tg-0pky">38.718</td>
    <td class="tg-0pky">31.587</td>
    <td class="tg-0pky">46.084</td>
  </tr>
  <tr>
    <td class="tg-c3ow" rowspan="3">user</td>
    <td class="tg-0pky">sentences per turn</td>
    <td class="tg-0pky">1.198</td>
    <td class="tg-0pky">1.233</td>
    <td class="tg-0pky">1.191</td>
    <td class="tg-0pky">0.512</td>
    <td class="tg-0pky">0.618</td>
    <td class="tg-0pky">0.51</td>
  </tr>
  <tr>
    <td class="tg-0pky">words per turn</td>
    <td class="tg-0pky">8.134</td>
    <td class="tg-0pky">8.977</td>
    <td class="tg-0pky">7.315</td>
    <td class="tg-0pky">7.844</td>
    <td class="tg-0pky">9.609</td>
    <td class="tg-0pky">7.023</td>
  </tr>
  <tr>
    <td class="tg-0pky">words per sentence</td>
    <td class="tg-0pky">6.537</td>
    <td class="tg-0pky">7.075</td>
    <td class="tg-0pky">5.903</td>
    <td class="tg-0pky"><span style="font-weight:400;font-style:normal">5.9</span></td>
    <td class="tg-0pky">6.964</td>
    <td class="tg-0pky">5.551</td>
  </tr>
  <tr>
    <td class="tg-c3ow" rowspan="3">agent</td>
    <td class="tg-0pky">sentences per turn</td>
    <td class="tg-0pky">1.8</td>
    <td class="tg-0pky">1.582</td>
    <td class="tg-0pky">1.712</td>
    <td class="tg-0pky">0.977</td>
    <td class="tg-0pky">0.648</td>
    <td class="tg-0pky">0.922</td>
  </tr>
  <tr>
    <td class="tg-0pky">words per turn</td>
    <td class="tg-0pky">15.61</td>
    <td class="tg-0pky">14.924</td>
    <td class="tg-0pky">16.061</td>
    <td class="tg-0pky">10.199</td>
    <td class="tg-0pky">11.033</td>
    <td class="tg-0pky">18.103</td>
  </tr>
  <tr>
    <td class="tg-0pky">words per sentence</td>
    <td class="tg-0pky">8.402</td>
    <td class="tg-0pky">8.477</td>
    <td class="tg-0pky">8.794</td>
    <td class="tg-0pky">5.239</td>
    <td class="tg-0pky">5.554</td>
    <td class="tg-0pky">10.138</td>
  </tr>
</tbody>
</table>

As shown in the table, the datasets look similar as mean lengths for dialogues, turns and sentences are close to each other. Moreover, the average length of user utterances across the datasets is similar, the same holds for agent utterances. Interestingly, that on average agents have longer sentences, it is because after user started conversation the agent usually ask specific questions to clarify the details, these questions are often have shot answers like :
- Agent: When do you want to ...?
- User: Tomorrow.
- Agent: How many ... ?
- User: Two.

In summary, the whole dataset seems to be reasonably large, however it would be better for training if it was about 2 times bigger.

Since the datasets were obtained from Amazon Mechanical Turk, there is no doubt that user utterances were generated by a human. However, it is not actually clear if the agent's responses were created by a computer or a human, since the sentences look very natural. If the agent's utterances were generated by a system then it should be a very advanced one, but i think that the whole dialogues were fully made up by a human, so they can be used to train good dialogue system. So despite the dataset size, the quality of the samples is great and it seems that this dataset is a good choice for creating a dialogue system. Moreover, presented data are very practically orientated so the final model should be a good system for booking tables at restaurants, ordering a taxi or buying tickets to the cinema. The only thing that will remain is a database of relevant cinemas, restaurants and taxi services :-).

## Licence
MICROSOFT LICENSE
## Reference
```
@article{li2018microsoft,
  title={Microsoft Dialogue Challenge: Building End-to-End Task-Completion Dialogue Systems},
  author={Li, Xiujun and Panda, Sarah and Liu, Jingjing and Gao, Jianfeng},
  journal={arXiv preprint arXiv:1807.11125},
  year={2018}
}

@article{li2016user,
  title={A User Simulator for Task-Completion Dialogues},
  author={Li, Xiujun and Lipton, Zachary C and Dhingra, Bhuwan and Li, Lihong and Gao, Jianfeng and Chen, Yun-Nung},
  journal={arXiv preprint arXiv:1612.05688},
  year={2016}
}
```

