# Introduction
This document briefs about the dataset - Semantic Parsing for Task Oriented Dialog Datasets created by Sonal et al. (2018). 
The metrics presented below describes the statistics of this dataset.
The dataset has utterances that are focused on navigation, events, and navigation to events.

To create the datasets, Sonal et al. (2018), have asked crowdsourced workers to generate natural language sentences that they would ask a system that could assist in navigation and event queries. 
These requests were then labeled by two annotators. If these annotations weren’t identical then they adjudicated with a third annotator. 
If all three annotators disagreed then they discarded the utterance and its annotations. 
63.40% of utterances were resolved with 2 annotations and 94.09% were resolved after getting 3 annotations. 
They also compared percentage of utterances that were resolved after 2 annotations for depth ≤ 2 (traditional slot filling) and for depth > 2 (compositional): 68.87% vs 62.03%, noting that the agreement rate is similar.

# Format (each row)

The format for each row is described below,

raw_utterance &lt;tab> tokenized_utterance &lt;tab> TOP-representation

where the TOP-representation is an annotated version of the utterance

e.g. Art fairs this weekend in Detroit &lt;tab> [IN:GET_EVENT [SL:CATEGORY_EVENT Art fairs ] [SL:DATE_TIME this weekend ] in [SL:LOCATION Detroit ] ]
Opening of a non-terminal node is marked by "[" (part of the non-terminal label), while a closing is marked by a standalone "]"

# License

Provided under the CC-BY-SA license.

# Details of the datasets

* total annotations: 44783
* intents: 25
* slots: 36
* split: randomly
* training utterances size: 31279
* validation utterances size: 4462
* test utterances size: 9042
* median (mean) depth of the trees: 2 (2.54)
* the median (mean) length of the utterances: 8 (8.93)
* entropy: 5 (5.12)

# My impressions on this data:

(a) Upon analysing closely this dataset, the data looks natural to me, where intent and slots are very clearly marked.
Mostly work on task oriented intent and slot-filling work has been restricted to one intent per query and one slot label per token, and thus cannot model complex compositional requests, for example, the representations used on the ATIS dataset Mesnil et al. (2013) and in the Dialog State Tracking Challenge Williams et al. (2016).
But a actual system can use a hierarchical representations with nested intents (available in the semantic parsing data-set), that can improve coverage of requests. 

For better understanding, let's consider one example, where a typical systems classify the intent of a query (e.g. GET_DIRECTIONS) and tag the necessary slots (e.g. San Francisco) Mesnil et al. (2013); Liu and Lane (2016). It is difficult for such representations to adequately represent nested queries such as “Driving directions to the Eagles game”, which is composed of GET_DIRECTIONS and GET_EVENT intents. But a hierarchical representation of a data-set having nested intent for such queries, which dramatically improves the expressive power while remaining accurate and efficient to annotate and parse.

(.IN:GET_DIRECTIONS. ‘Driving directions to’ (.SL:DESTINATION. (.IN:GET_EVENT. ‘the’ (.SL:NAME_EVENT. ‘Eagles’) (.SL:CAT_EVENT. ‘game’) ) ) )

(.IN:GET_DISTANCE. ‘How far is’ (.SL:DESTINATION. (.IN:GET_RESTAURANT_LOCATION. ‘the’ (.SL:TYPE_FOOD. ‘coffee’ ) ‘shop’ ) ) )

The above example is for TOP annotations of utterances. Intents are prefixed with IN: and slots with SL:. In a traditional intent-slot system, the SL:DESTINATION could not have an intent nested inside it. This clearly indicates the extensive usefulness of this data-set in complex intent-slot based dialog systems.

Also other attractiveness of this data-set for the usage in the actual system are	: 
(i) The data-set representation simply denotes labeling spans of a sentence, which is much more straightforward than alternatives such as creating a logical form for the sentence, or an arbitrary dependency graph.
(ii) This data-set representation closely resembles syntactic trees representation, for this it can be easily used by the models from the large literature on constituency parsing.



(b) The semantic parsing literature has focused on representing language with logical forms Liang (2016); Zettlemoyer and Collins (2012); Kwiatkowski et al. (2010). Logical forms in data are more expressive than this data-set representation, as they are less tightly coupled to the input query, and can be executed directly. This is one of the drawbacks of this data-set but again logical forms are difficult to annotate and no large-scale datasets with logical forms are available.

Another drawback of this data is that it is well suited for tree-structured representation, but for arbitrary graphs, such as Abstract Meaning Representation (Banarescu et al., 2013) and Alexa Meaning Representation (Fan et al., 2017) this data is not very suitable. The limitation of this data as per my knowledge could be how these approaches (Banarescu et al., 2013; Fan et al., 2017) can represent complex constructions that this data may fail to do so.

# References

1. Laura Banarescu, Claire Bonial, Shu Cai, Madalina Georgescu, Kira Griffitt, Ulf Hermjakob, Kevin Knight, Philipp Koehn, Martha Palmer, and Nathan Schneider. 2013. Abstract meaning representation for sembanking. In Proceedings of the 7th Linguistic Annotation Workshop and Interoperability with Discourse, pages 178–186.
2. Xing Fan, Emilio Monti, Lambert Mathias, and Markus Dreyer. 2017. Transfer learning for neural semantic parsing. In Proceedings of the 2nd Workshop on Representation Learning for NLP, ACL.
3. Liang (2016) Percy Liang. 2016. Learning executable semantic parsers for natural language understanding. Commun. ACM, 59(9):68–76.
4. Zettlemoyer and Collins (2012) Luke S Zettlemoyer and Michael Collins. 2012. Learning to map sentences to logical form: Structured classification with probabilistic categorial grammars. arXiv preprint arXiv:1207.1420.
5. Kwiatkowski et al. (2010) Tom Kwiatkowski, Luke Zettlemoyer, Sharon Goldwater, and Mark Steedman. 2010. Inducing probabilistic ccg grammars from logical form with higher-order unification. In Proceedings of the 2010 conference on empirical methods in natural language processing, pages 1223–1233. Association for Computational Linguistics.
6. Mesnil et al. (2013) Grégoire Mesnil, Xiaodong He, Li Deng, and Yoshua Bengio. 2013. Investigation of recurrent-neural-network architectures and learning methods for spoken language understanding. In Interspeech.
7. Williams et al. (2016) Jason Williams, Antoine Raux, and Matthew Henderson. 2016. The dialog state tracking challenge series: A review. Dialogue & Discourse, 7(3):4–33.
