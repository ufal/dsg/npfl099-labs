### Topical-Chat: Towards Knowledge-Grounded Open-Domain Conversations


- What kind of data it is (domain, modality)
  The data is knowledge-grounded dialogue spanning 8 topics .
  It is centered on scraped Washington Post articles, where they choose top 3 entities for each article.
  Agents are then given additional knoweldge sources in form of wikipedia texts about the entities and reddit
  crowdsourced fun-facts about them. Which they are supposed to use during the converastion.
  Dataset is designed for converastional dialogue with no predefined goals.
  Dialogues are quality annotated at the end by agents themselves on 1-5 scale, in addition they also
   rate the quality of responses they recieve on a 1-5. Sentiemnt is mannualy self-annotated on a 1-8 scale.
    Agents also specify the knowledge source they used to generate their message.
  Data is stored in Json dicts' looking like this:
```
  <conversation_id>: {
  	“article_url”: <article url>,
  	“config”: <config>, # one of A,B,C, D
  	“content”: [ # ordered list of conversation turns
  		{
  		“agent”: “agent_1”, # or “agent_2”,
  		“message” : <message text>,
  		“sentiment”: <text>,
  		“knowledge_source” : [“AS1”, “Personal Knowledge”,...],
  		“turn_rating”: “Poor”, # Note: changed from number to actual annotated text
  		},…
  	],
  	“conversation_rating”: {
  		“agent_1”: “Good”,
  		“agent_2”: “Excellent”
  		}
```
The whole dataset is made available under the  MIT license.



The data is split into 5 distinct groups: Train, Valid Frequent, Valid Rare, Test Frequent and Test Rare.
Frequent set contains entities frequently seen in the training set.
Rare set contains entities that were infrequently seen in the training set.




### Data Statistics:
|                   | Train |
| ----              | ----  |
|# dialogues   | 8628  |
|# turns       | 188378 |
|# words    | 3731296|
|# sentences|  383346|
| vocab size | 35615 |
|std-dev turns  | 1.7490421689402638  |
|std-dev sentences |13.36232860689309 |
|std-dev words | 125.52498360158182 |
|average # turns per conversation  | 21.8 |
|average # tokens of utterance    | 19.5  |
|average # words per dialogue|432.46360686138155|


char level Entropy: 4.465064467696501

For word level entropy I tokenize with pretrained BERT tokenizer.
So its technically subword level Entropy.
https://huggingface.co/transformers/tokenizer_summary.html

(sub)word level Entopy:  6.3667205506479725


```

```

I think the data looks natural in the language modelling sense, but learning and generalizing from this dataset is incredibly difficult.
I feel like this dataset is just too artifical to learn to model general dialogue. What I am lacking is some kind of
mechanism to lookup the knowledge automatically, like using google search to find relevant knowledge.
 But making something like this differentiable would be hell. Personally I feel like this dataset is very focused on benchmarking on the test set.
But this is a problem with the field of open domain dialogue systems, this task is just too hard with the resources/knowledge we have.
Focusing on task oriented dialogue and systems that can solve small tasks for you like alexa/ google home is more realistic.  
