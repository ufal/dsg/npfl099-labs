# PolyAI Task-specific Banking

## Description

###  What kind of data is it?
The dataset consists of banking task-oriented user queries labeled with 77 different intents split into train and test sets.

### How it was collected? What kind of annotation is present?
It is not explicitly stated in the paper, but I'm quite sure that they collected actual user queries and then labeled them.

### What kind of dialogue system or dialogue system component it's designed for?
It is designed for task-oriented conversational systems (for intent detection).

### What format is it stored in?
The data is stored in csv and the intents are stored in json.
The csv consists of 2 columns (text and category), where text is query and category is coresponding intent.

### License
Creative Commons Attribution 4.0 International

Permissions: commercial use, modification, distribution, private use

Limitations: liability, trademark use, patent use, warranty

## Measurements

### Test
- number of queries: 3080
- number of words: 33734
- mean query lenght (in words): 10.95
- std of query lenght: 6.68
- vocabulary size: 1414
- entropy: 7.67

### Train
- number of queries: 10003
- number of words: 119530
- mean query lenght (in words): 11.94
- std of query lenght: 7.89
- vocabulary size: 2381
- entropy: 7.77

### Combined test+train
- number of queries: 13083
- number of words: 153264
- mean query lenght (in words): 11.71
- std of query lenght: 7.64
- vocabulary size: 2602
- entropy: 7.77

## Impression
The data looks narural but some of the queries greatly differ in length (longest one is 79 words). I think that it should not be hard to learn from this dataset since it is single-domain and well labeled.
