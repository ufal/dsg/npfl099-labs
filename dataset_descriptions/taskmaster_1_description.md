# [ Taskmaster-1 (Toward a Realistic and Diverse Dialog Dataset)](https://research.google/pubs/pub48484/)


## Dataset Description

### Domain
The conversations present in the dataset fall into one of the following domains:
- ordering pizza
- creating auto repair appointments
- setting up ride service
- ordering movie tickets
- ordering coffee drinks and making restaurant reservations

## Data Collection Methodology
Two data collection methodologies were used in order to produce the Taskmaster-1 dataset, each with it's advantages and disadvantages. More specifically:

### WOz (Wizard of Oz) Approach
- The first methodology used utilizes the Wizard of Oz approach. In this methodology trained agents and crowd sourced workers interact (users) to complete a task (based on the domain in question). The users are, however, under the impression that they are interacting with an automated system instead of actual human respondents. Additionally, the use of Text to Speech and delayed turn taking prevents the conversation overly "human". 
- The authors claim that this "creates an idealized spoken environment, revealing how users would openly and candidly express themselves with an automated assistant that provided superior natural language understanding".  

### Self-Dialogue Approach
- The second methodology involves the crowd sourced workers yet again. 
- However, this time they are responsible for designing the entire dialogue themselves. That is, they play the role of both the user and the dialogue system.  
- The workers are not restricted to any (detailed) scripts or to a (small) knowledge base - - The authors of the dataset (and the corresponding paper) claim that the this implies that the dataset contains more realistic and diverse conversations in comparison to existing datasets.
- The clear benefit is that since the dialogue is produced by a single person there are no misunderstandings in the dialogue and the conversation is more fluid.

## Annotation
The annotation scheme is rather simple. More specifically, for each task the conversations is labeled with the API arguments such as for a pizza order the store name, number of pizzas, sizes, topping(s) and sometimes crust type is provided.
In addition to the parameter values, the success or failure of the transaction itself is labeled with 'accept' or 'reject'. If the dialog only makes general reference to a transaction, e.g. 'OK your pizza has been ordered' or 'Sorry we just discontinued that pizza so it's no longer available', the label 'accept' or 'reject' is added to the vertical name, i.e. 'pizza_ordering.accept'. Where the vertical name refers to the task/domain into which the particular dialogue falls. In the example this would be the goal of ordering a pizza. This is in contrast to the labels being added to individual parameters. In the context of the pizza ordering task, an instance of this would be the sentence 'It looks like we are out of pepperoni tonight', where the token 'pepperoni' would be labeled 'type.topping.reject'.

## Format
The corpus is stored in JSON format within two files. 

- self-dialogs.json (Contains all one-person dialogues)
- woz-dialogs.json (Contains WOz dialogues)

Additionally the IDs are split into 3 CSVs corresponding to train/dev/test sets.


## License
Creative Commons Attribution 4.0 License

## Data Length

The word counts include punctuation*

|                                                	| Self-dialogues 	| MultiWOZ 	|
|------------------------------------------------	|-------------------|-----------|
| dialogues                                      	| 7 708        	    | 5 507    	|
| turns (repetition by same speaker counted)     	| 169 469       	| 133 597   |
| turns (repetition by same speaker not counted) 	| 85 693        	| 71 959    |
| sentences                                      	| 178 985      	    | 159 162  	|
| words                                          	| 1 738 121         | 1 267 601 |
| vocabulary*                                     	| 26 992            | 17 640    |


## Means/Standard Deviations
This question is rather ambiguously phrased. It can refer to the mean/std of either the counts of the quantities listed or rather it can mean the character length of the given quantities. I chose to use the latter as the former is in my opinion not well defined for all the quantities required. The statistics relating to words are understandably due to the fact that the tokens include punctuation.

|                                            	| Self-dialogues   	| MultiWOZ        	|
|--------------------------------------------	|----------------	|-----------------	|
| dialogues                                  	| 980.8 (352.51) 	| 970.73 (431.71) 	|
| turns (repetition by same speaker counted) 	| 43.61 (34.8)   	| 39.01 (34.47)   	|
| sentences                                  	| 41.18 (43.37)  	| 32.59 (27.21)   	|
| words                                      	| 3.51 (2.17)    	| 3.42 (2.21)     	|

## Entropy

Tokens include punctuation.

|         	| Self-dialogues 	| MultiWOZ 	|
|---------	|----------------	|----------	|
| Entropy 	| 6.04           	| 5.78     	|


## Dataset Overview
In the following we provide an overview of the dataset described above. In particular, we will focus on its characteristics with respect to the domains it was designed to cover instead of a more broad critique.

In order to asses how realistic the conversations in the dataset feel, we first need to get a bit more granular. More specifically, the utterances themselves do feel natural. As they are, for both data collection methodologies, created by human agents, who were not given strict limitations on how to structure the dialogue. They contain colloquially used shorthands such as "cuz" (because) "u" (you) and even disfluencies such as "hmm" and "umm". The authenticity begins to waiver a bit if we consider the entire conversations, for those conversations that were created by the "self-dialogue" methodology. These conversations feel a bit out of touch with reality as the exchanges contain, next to none of the, all too common, misunderstandings.

The question of learning difficulty from this particular dataset is hard to answer. The dataset is rather realistic and the annotations are of high quality, however the feasibility of training a dialogue system relies on much more than these qualities, such as the knowledge base available. Nevertheless, given my limited knowledge on the topic I would assume that as available datasets go, it is a well crafted one and should provide a solid base for learning. 

Questions have been raised as it pertains to the usability of the Taskmaster-1 dataset in practice. In particular the authors of the paper [Where is the context? – A critique of recent dialogue datasets](https://deepai.org/publication/where-is-the-context-a-critique-of-recent-dialogue-datasets) express two main points of criticism. 
- The dataset contains dialogues, where different actions follow from the same dialogue history and thus the system is not able to learn "deterministic" behavior.  
- The dataset is history-independent, meaning that the systems trained using the dataset do not perform significantly better if more of the past dialogue history is available.

I would say that I find the arguments presented in the paper convincing. Especially, the hypothesis that the cause of the second identified pain point is the fact, that the human creators of the dataset are asked to pretend that they want to achieve a goal. As such, they are not actually interested in the information they obtain, but rather seek to complete each dialogue as soon as possible. 