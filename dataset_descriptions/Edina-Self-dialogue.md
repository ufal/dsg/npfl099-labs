# Edina Self-dialogue

Github: https://github.com/jfainberg/self_dialogue_corpus 
Papers: https://arxiv.org/pdf/1809.06641.pdf  
        http://alexaprize.s3.amazonaws.com/2017/technical-article/edina.pdf


## Introduction

This documento is an overview of Edina, which is a conversation agent that uses data collected from Amazon Mechanical Turk (AMT), using a technique called self-dialogues.

## Data Collection -  Self-dialogue Corpus

This corpus was collected using Amazon Mechanical Turk (AMT). For this, Workers were asked to create a fictitious two-party conversation around a topic, in which they had to fill in 20 text boxes. In total, only eight out of 2,717 workers were banned and 145 conversations (≈ 0.6%) were rejected. The only rejection criteria related to abusing the system, such as submitting (near) duplicate entries or content with exaggerated bad language, so they flagged conversations which contained a large number of words from a badwords list. 

## Details of the datasets
The corpus consists of 23 folders, totaling 35.5 MB, each with a specific topic. Each CSV file is composed of 50 columns that contain information such as ID, Title, Description, Keywords, WorkerID, Assignment Status, Sentences, among others. Also, there is a list file (blocked_workers.txt) of workers who did not comply with the requirements of the tasks, these are omitted by default.

| Category | Count 
| ------ | ------ | 
| Topics | 23
| Conversations | 24,283
| Words | 3,653,313
| Turns | 141,945 
| Unique users | 2,717
| Mean of words | 150,44
| Mean of turns | 5,84
| Avg. conversation per user | ~9
| Peak conversation per day | 2,307
| Unique tokens | 117,068
| conversation entropy | 9.05


Topics are divided in:

| Topic | Conversations | Words | Turns 
| ------ | ------ | ------ | ------ | 
| === | === | === | ===
| Movies | 4,126 | 814,842 | 82,018
| Action | 414 | 37,037 | 4,140
| Comedy | 414 |  36,401 | 4,140
| Fast & Furious | 343 | 33,964 | 3,430
| Harry Potter | 414 |  44,220 | 4,140
| Disney | 2,331 | 232,573 | 23,287
| Horror | 414 | 428,33 | 4,138
| Thriller | 828 | 77,975 | 8,277
| Star Wars | 1,726 | 178,351 | 17,260
| Superhero | 414 | 40,967 | 4,140
| === | === | === | ===
| Music | 4,911 | 924,993 | 98,123
| Pop | 684 | 62,383 | 6,840
| Rap/Hip-Hop | 684 | 66,376 | 6,840
| Rock | 684 | 63,349 | 6,837
| The Beatles | 679 | 68,396 | 6,781
| Lady Gaga | 558 | 49,313 | 5,566
| === | === | === | ===
| Music and Movies | 216 | 37,303 | 4,320
| === | === | === | ===
| Transition Mus-Mov | 198 | 4,287 | 396
| === | === | === | ===
| Baseball | 496 | 99,298 | 9,881
| Basketball | 485 | 95,264 | 9,507
| Ice Hockey | 174 | 34,288 | 3,417
| NFL Football | 2,801 | 562,801 | 55,939
| === | === | === | ===
| Fashion | 210 | 46,099 | 4,105



## Example self-dialogues

24,165 text files containing conversations across 23 topics were generated, totaling 19.0 MB.

Below is shown, as example, two dialogs generated:

1. “Do you like Lady Gaga's music?
Yes, I think she is fantastic.
Me, too. What is your favorite of her songs?
I think my favorite Lady Gaga song is "Love Game", because it is so fun. How about you?
I like "Alejandro", because of the music video. Are there any Lady Gaga songs you don't like?
I'm not very fond of "The Edge of Glory". It sounds kind of generic to me.
Really? I liked "The Edge of Glory" a lot--I found it inspirational.
Personally, I found "Born This Way" more inspirational than "The Edge of Glory".
I can see that. "Born This Way" is great for encouraging people to be themselves.
I agree. That's why I like it a lot.”

2. “You know what bugs me?
What's that?
Whenever you see Rapunzel, why does she have long hair again?
What do you mean?
So Flynn Rider cut her hair off in the movie, but it's always long in the cartoons again.
Ah, I see.  Guess it's better marketing.
But it totally breaks the canon of the series.
Do you really think kids care about that stuff?
My five year old noticed it!
Mine was too busy dancing around to the music.”

## License
BSD 3-Clause License

Copyright (c) 2017,
All rights reserved.

Redistribution and use in source and binary forms are permitted provided that the following conditions:
1. Source code - retain the above copyright notice, the list of conditions and the disclaimer.
2. Binary form - retain the above copyright notice, the list of conditions, the disclaimer and/or other materials provided with the distribution.
3. "Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from  this software without specific prior written permission."

## Impression

In general, the dataset looks natural in the language modelling sense, but I don't feel that this dataset is entirely natural. In some conversations, some sentences responses sounds redundant and superficial, but in others it is possible to notice a deepening on the subject, what turn the dialogue interesting. Having an overview, the dataset has simple conversation, that can be easy to understand, the sentences are not so complex. 
I think this dataset has the potential to make chatbots, especially for being able to generate conversations related to music and movies in a deeper way.

## References

```
@article{fainberg2018talking,
  title={Talking to myself: self-dialogues as data for conversational agents},
  author={Fainberg, Joachim and Krause, Ben and Dobre, Mihai and Damonte, Marco and Kahembwe, Emmanuel and Duma, Daniel and Webber, Bonnie and Fancellu, Federico},
  journal={arXiv preprint arXiv:1809.06641},
  year={2018}
}
@article{krause2017edina,
  title={Edina: Building an Open Domain Socialbot with Self-dialogues},
  author={Krause, Ben and Damonte, Marco and Dobre, Mihai and Duma, Daniel and Fainberg, Joachim and Fancellu, Federico and Kahembwe, Emmanuel and Cheng, Jianpeng and Webber, Bonnie},
  journal={Alexa Prize Proceedings},
  year={2017}
}
```
